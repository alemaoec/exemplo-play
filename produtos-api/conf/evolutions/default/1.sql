# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table cliente (
  codigo                        varchar(255),
  nome                          varchar(255),
  cpf                           varchar(255)
);

create table produto (
  codigo                        varchar(255),
  titulo                        varchar(255),
  tipo                          varchar(255),
  descricao                     varchar(255),
  preco                         double
);


# --- !Downs

drop table if exists cliente;

drop table if exists produto;

