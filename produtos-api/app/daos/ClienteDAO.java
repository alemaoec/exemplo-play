package daos;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.avaje.ebean.ExpressionList;
import com.avaje.ebean.Model.Finder;

import models.Cliente;


public class ClienteDAO {
  private Finder<Long, Cliente> clientes = new Finder<>(Cliente.class);

  public List<Cliente> todos() {
    return clientes.all();
  } 
}