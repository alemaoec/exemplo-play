package controllers;

import javax.inject.Inject;
import java.util.List;

import play.data.Form;
import play.data.FormFactory;
import play.mvc.*;
import views.html.*;

import daos.ClienteDAO;
import models.Cliente;

public class ClienteController extends Controller {
  @Inject
  private ClienteDAO clienteDAO;
  @Inject
  private FormFactory formularios;

  public Result listarClientes() {
    List<Cliente> listaClientes = clienteDAO.todos();
    return ok(viewClientes.render(listaClientes));
  }

  public Result formularioCliente() {
    return ok(fomularioCliente.render("Cadastrar Cliente"));
  }

  public Result criarCliente() {
    Form<Cliente> formulario = formularios.form(
                                Cliente.class).bindFromRequest();
    Cliente cliente = formulario.get();
    cliente.save();
    return redirect(routes.ClienteController.listarClientes());
  }
}