package models;

import java.util.*;
import javax.persistence.*;

import com.avaje.ebean.Model;
import play.data.format.*;
import play.data.validation.*;
import javax.persistence.Entity;
  
@Entity
public class Cliente extends Model {
  String codigo;
  String nome;
  String cpf;
  

  public String getNome() {
    return nome;
  }

  public String getCpf() {
    return cpf;
  }

  public String getCodigo() {
    return codigo;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public void setCpf(String cpf) {
    this.cpf = cpf;
  }

  public void setCodigo(String codigo) {
    this.codigo = codigo;
  }
}